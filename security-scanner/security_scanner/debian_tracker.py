#!/usr/bin/env python3

import json
import apt_pkg
import urllib.request
import os.path

class DebianTracker(object):
    _debian_security = {}
    _distro = None

    def __init__(self, distro=None, forceUpdate=False):
        apt_pkg.init_system()

        self._distro = distro
        if forceUpdate or (not os.path.exists("debian-security.json")):
            self.update()

        with open('debian-security.json') as f:
            self._debian_security = json.load(f)

    def update(self):
        urllib.request.urlretrieve("https://security-tracker.debian.org/tracker/data/json", "debian-security.json")

    def getLatestVersion(self, package, distro=None):
        if distro is None:
            if self._distro is None:
                raise ValueError('you must pass distro, either to the function or on creating the object')
            else:
                distro = self._distro

        latest_version = None
        if package in self._debian_security:
            for cve in self._debian_security[package]:
                if distro in self._debian_security[package][cve]['releases']:
                    for repository in self._debian_security[package][cve]['releases'][distro]['repositories']:
                        version = self._debian_security[package][cve]['releases'][distro]['repositories'][repository]
                        if latest_version is not None:
                            if apt_pkg.version_compare(version,latest_version) > 0:
                                latest_version = version
                        else:
                             latest_version = version
                else:
                    return 'distro not found'

            return latest_version
        else:
            return 'package not found'

    def checkPackage(self, package, version, distro=None):
        result = {
                'current': version,
                'latest': None,
                'comparison': None,
                'message': '',
            }
        latest_version = self.getLatestVersion(package, distro)
        if latest_version == 'package not found' or latest_version == 'distro not found':
            result['message'] = latest_version
        else:
            result['latest'] = latest_version
            result['comparison'] = apt_pkg.version_compare(version, latest_version)
        return result
