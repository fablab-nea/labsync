#!/usr/bin/env python

import gitlab

from security_scanner.file_writer import FileWriter

class GitLab:
    def __init__(self, gitlab_url, project_id, api_token=None):
        if gitlab_url is None:
            raise ValueError('must pass gitlab_url')
        if project_id is None:
            raise ValueError('must pass project_id')

        self._gl = gitlab.Gitlab(gitlab_url, private_token=api_token)
        self._project = self._gl.projects.get(project_id)

    def getLastSuccessfulJob(self, ref, name):
        pipelines = self._project.pipelines.list(ref=ref, status='success', as_list=False)

        last_successful_job = None
        for pipeline in pipelines:
            jobs = pipeline.jobs.list(scope='success', as_list=False)
            for job in jobs:
                if job.attributes['name'] == name:
                    return job

        return None

    def downloadArtifact(self, job, sourcePath, destPath):
        job_id = job.attributes['id']
        print("Downloading artifact {} for job #{}".format(sourcePath, job_id))
        target = FileWriter(destPath)
        artifact = self._project.jobs.get(job_id).artifact(sourcePath, streamed=True, action=target)
        del(target)

    def createPipeline(self, ref):
        pipeline = self._project.pipelines.create({'ref': ref})
