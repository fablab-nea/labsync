#!/usr/bin/env python3

import re
import os.path
#from pprint import pprint


class DpkgList(object):
    _distro = None

    def __init__(self, distro):
        self._distro = distro

    def parseFile(self, path='.', filename=None, encoding="utf-8"):
        if filename is None:
            filename = 'debian-' + self._distro + '.dpkg-list'

        dpkg_list = None
        with open(os.path.join(path, filename), encoding=encoding) as f:
            dpkg_list = f.readlines()

        installed_packages = {}

        for item in dpkg_list:
            matches = re.match(r"ii\s+(\S+)\s+(\S+)\s+(\S+)\s+(.*)$", item)
            if matches is not None:
                package = matches.group(1)
                installed_packages[package] = {}
                installed_packages[package]['version'] = matches.group(2)
                installed_packages[package]['arch'] = matches.group(3)
                installed_packages[package]['description'] = matches.group(4)

        return installed_packages
