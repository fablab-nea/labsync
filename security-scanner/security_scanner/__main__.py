#!/usr/bin/env python3

import os
import sys

from security_scanner.debian_tracker import DebianTracker
from security_scanner.dpkg_list import DpkgList
from security_scanner.gitlab import GitLab

from pprint import pprint

def stripArch(package):
    return package.split(":", 2)[0]

def checkDebianDistro(distro):
    result = 0
    pkglist = DpkgList(distro)
    packages = pkglist.parseFile()
    tracker = DebianTracker(distro)

    for package in packages:
        state = tracker.checkPackage(stripArch(package), packages[package]['version'])
        if 'comparison' in state and state['comparison'] is not None and state['comparison'] < 0:
            print("Package: {}, current: {}, latest: {}, comparison: {}".format(package,state['current'], state['latest'], state['comparison']))
            result = 1
    print("checked {} packages; result: {}".format(len(packages), result))

    return result

if __name__ == '__main__':
    gitlab_url = os.environ.get('GITLAB_URL')
    project_id = os.environ.get('CI_PROJECT_ID')
    api_token = os.environ.get('PRIVATE_TOKEN')
    gitlab = GitLab(gitlab_url, project_id, api_token)
    for distro in sys.argv[1:]:
        job = gitlab.getLastSuccessfulJob('main', 'squashfs_main')
        if job is not None:
            gitlab.downloadArtifact(job, 'images/debian-' + distro + '.dpkg-list', 'debian-' + distro + '.dpkg-list')
            if checkDebianDistro(distro) > 0:
                ref = job.attributes['ref']
                print("creating pipeline for reference {}".format(ref))
                pprint(job.attributes)
                gitlab.createPipeline(ref)
        else:
            print('last successful job not found')
            sys.exit(1)


