#!/usr/bin/env python3

class FileWriter(object):
    def __init__(self, filename):
        self._fd = open(filename, 'wb')

    def __call__(self, chunk):
        self._fd.write(chunk)
