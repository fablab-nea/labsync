import setuptools

setuptools.setup(
    name='security_scanner',
    version='1.2.0',
    packages=setuptools.find_packages(),
    install_requires=[
        'python-gitlab==1.7.0',
        'urllib3==1.24.1',
    ],
)
