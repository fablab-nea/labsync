#!/bin/sh
boot_partition="/dev/${LABSYNC_DISK}1"
localboot="$(sed -n 's/^.*labsync_localboot=\([^ ]*\).*$/\1/p' /proc/cmdline)"

if [ -z "$localboot" ]; then
	umount "$boot_partition" || true
	if ! grep -q ' /boot ' /etc/fstab; then
		echo "$boot_partition /boot ext2 defaults 0 0" >> /etc/fstab
	fi
	mke2fs -t ext2 -F "$boot_partition"
	mount /boot
	rsync -av /usr/local/boot/ /boot/
	grub-install /dev/$LABSYNC_DISK
	sed -i 's/^\(GRUB_DEVICE=\).*$/\1""/' /usr/sbin/grub-mkconfig
	#sed -i 's/^\(GRUB_CMDLINE_LINUX_DEFAULT=\).*$/\1'"'quiet boot=labsync labsync_localboot=\"$LABSYNC\"'/" /etc/default/grub
	sed -i 's/^\(GRUB_CMDLINE_LINUX=\).*$/\1'"'boot=labsync labsync_localboot=\"$LABSYNC\"'/" /etc/default/grub
	sed -i 's/^\(GRUB_TIMEOUT\)=.*$/\1=1/' /etc/default/grub
	update-grub
fi
