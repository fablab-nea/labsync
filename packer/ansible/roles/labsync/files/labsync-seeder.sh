#!/bin/sh

set -e

cd /var/lib/labsync

rm -f "$LABSYNC_SQUASHFS_FILE"
ln -s "$LABSYNC_SQUASHFS_LV" "$LABSYNC_SQUASHFS_FILE"

exec aria2c \
	--allow-overwrite \
	--seed-ratio=0.0 \
	--summary-interval=60 \
	--file-allocation=none \
	--enable-dht=false \
	--force-save \
	"$LABSYNC_TORRENT"
