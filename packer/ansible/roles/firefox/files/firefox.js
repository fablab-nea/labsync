// debian settings
pref("extensions.update.enabled", true);
pref("intl.locale.requested", "");
pref("browser.shell.checkDefaultBrowser", false);

// disable trackers
lockPref("app.normandy.enabled", false);
lockPref("browser.chrome.errorReporter.enabled", false);
lockPref("browser.safebrowsing.downloads.enabled", false);
lockPref("browser.safebrowsing.downloads.remote.enabled", false);
lockPref("browser.safebrowsing.malware.enabled", false);
lockPref("browser.safebrowsing.passwords.enabled", false);
lockPref("browser.safebrowsing.phishing.enabled", false);
lockPref("browser.tabs.crashReporting.sendReport", false);
lockPref("datareporting.healthreport.uploadEnabled", false);
lockPref("datareporting.policy.dataSubmissionEnabled", false);
lockPref("security.ssl.errorReporting.enabled", false);

// design
pref("browser.newtabpage.enabled", false);
pref("browser.uiCustomization.state", '{"placements":{"widget-overflow-fixed-list":[],"PersonalToolbar":["personal-bookmarks"],"nav-bar":["back-button","forward-button","home-button","urlbar-container","stop-reload-button","downloads-button","library-button"],"TabsToolbar":["tabbrowser-tabs","new-tab-button","alltabs-button"],"toolbar-menubar":["menubar-items"]},"seen":["developer-button"],"dirtyAreaCache":["PersonalToolbar","nav-bar","TabsToolbar","toolbar-menubar"],"currentVersion":14,"newElementCount":3}');

// for experienced users
pref("browser.urlbar.trimURLs", false);
pref("browser.fixup.alternate.enabled", false);

// privacy
pref("privacy.donottrackheader.enabled", true);

pref("privacy.history.custom", true);
pref("places.history.enabled", false);
pref("browser.formfill.enable", false);

// search
pref("browser.search.hiddenOneOffs", "Google,Amazon.de,Bing,Debian packages,DuckDuckGo,eBay,Ecosia,LEO Eng-Deu,Wikipedia (de)"); // hide „one click“ search eingines
pref("browser.search.suggest.enabled", false);
pref("browser.urlbar.placeholderName", "DuckDuckGo"); // defaults to google, even if DuckDuckGo is the default
