#!/bin/sh

vg='vglab'

# nothing needs to be done if previous data in overlay present
[ -d /var/lib/docker ] && exit 0

# remove LV if exists
if lvs $vg/docker-thinpool > /dev/null 2>&1; then
	lvremove -y $vg/docker-thinpool
fi

if ! lvs -o layout --noheadings $vg/docker-thinpool | grep -q 'thin,pool'; then

	if ! lvs -o LV_NAME --noheadings | grep -q docker-thinpool; then
		lvcreate --wipesignatures y --yes -n docker-thinpool "$vg" -l 10%VG
	fi

	if ! lvs -o LV_NAME --noheadings | grep -q docker-thinpoolmeta; then
		lvcreate --wipesignatures y --yes -n docker-thinpoolmeta "$vg" -l 1%VG
	fi

	lvconvert -y \
		--zero n \
		-c 512K \
		--thinpool "$vg/docker-thinpool" \
		--poolmetadata "$vg/docker-thinpoolmeta"
fi

if ! [ -f /etc/lvm/profile/docker-thinpool.profile ]; then
	[ -d /etc/lvm/profile ] || mkdir /etc/lvm/profile
	cat > /etc/lvm/profile/docker-thinpool.profile << EOF
activation {
  thin_pool_autoextend_threshold=80
  thin_pool_autoextend_percent=20
}
EOF
	lvchange --metadataprofile docker-thinpool "$vg/docker-thinpool"
	lvs -o+seg_monitor
fi

lv_path="${vg}-docker--thinpool"

if ! [ -d /etc/docker ]; then
	mkdir /etc/docker
fi
cat > /etc/docker/daemon.json << EOF
{
    "storage-driver": "devicemapper",
    "storage-opts": [
    "dm.thinpooldev=$lv_path",
    "dm.use_deferred_removal=true",
    "dm.use_deferred_deletion=true"
    ]
}
EOF
mkdir -m 711 /var/lib/docker
