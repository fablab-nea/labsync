#!/bin/sh
set -e
set -o nounset

export NAME="$1"
mkdir -p images
export IMAGES="$(realpath images)"

(
    cd packer

    python3 -c 'import sys, yaml, json; json.dump(yaml.safe_load(sys.stdin), sys.stdout, indent=4)' < "$NAME.yaml" > "$NAME.json"

    packer build "$NAME.json"
)

echo "$NAME" >> images.txt
