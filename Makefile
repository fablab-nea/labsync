PACKER_VERSION ?= 1.5.1
ANSIBLE_VERSION ?= 2.9.2
WEBSEED ?= http://10.2.2.1

CI_COMMIT_REF_SLUG ?= $(shell git name-rev --name-only HEAD)

DOCKER_IMAGE_BUILDER ?= labsync-builder:$(CI_COMMIT_REF_SLUG)
DOCKER_IMAGE_SECURITY_SCANNER ?= security-scanner:$(CI_COMMIT_REF_SLUG)

CWD=$(abspath $(patsubst %/,%,$(dir $(abspath $(lastword $(MAKEFILE_LIST))))))

COMPRESSION_LEVEL ?= 5

ANSIBLE_LIMIT ?= qemumachine
ANSIBLE_TAGS ?=

qemu_ifname_br = brlabsync
qemu_ifname_tap = taplabsync
qemu_hostname = qemumachine
qemu_host_ip = 10.2.2.1
qemu_network = 10.2.2.0
qemu_netmask = 24
qemu_vm_ip = 10.2.2.10
qemu_disk = tmp/qemu-disk.img
qemu_target ?= debian-bookworm
qemu_kernel = $(qemu_target).linux
qemu_torrent = $(qemu_target).torrent
qemu_initramfs = $(shell \
if [ -e images/$(qemu_target).initramfs.dev ] && [ `date -r images/$(qemu_target).initramfs.dev +%s` -gt `date -r images/$(qemu_target).initramfs +%s` ]; then \
	echo "$(qemu_target).initramfs.dev"; \
else \
	echo "$(qemu_target).initramfs"; \
fi \
)
ci_environment=$(shell env | sed -n 's/^\(CI_.*\)=.*/-e \1/p')

.PHONY: default
default: builderimg images/debian-bookworm.squashfs

.PHONY: clean
clean:
	rm -f images/*
	rm -rf tmp

.PHONY: builderimg
builderimg:
	docker build \
		--pull \
		-t "$(DOCKER_IMAGE_BUILDER)" \
		--cache-from "$(DOCKER_IMAGE_BUILDER)" \
		--build-arg "PACKER_VERSION=$(PACKER_VERSION)" \
		--build-arg "ANSIBLE_VERSION=$(ANSIBLE_VERSION)" \
		builder

.PHONY: secscanimg
secscanimg:
	docker build --pull -t "$(DOCKER_IMAGE_SECURITY_SCANNER)" --cache-from "$(DOCKER_IMAGE_SECURITY_SCANNER)" security-scanner

images:
	[ ! -d "$@" ] && mkdir "$@"
	touch "$@"

images/debian-bookworm.squashfs: images
	docker run \
		--rm \
		-v /var/run/docker.sock:/var/run/docker.sock \
		-v "${PWD}:${PWD}" \
		-w "${PWD}" \
		-e "IMAGES=${PWD}/images" \
		-e "WEBSEED=$(WEBSEED)" \
		-e "COMPRESSION_LEVEL=$(COMPRESSION_LEVEL)" \
		$(ci_environment) \
		"$(DOCKER_IMAGE_BUILDER)" \
		scripts/packer.sh \
		debian-bookworm

images/debian-bookworm.torrent: images
	docker run \
		--rm \
		-v "${PWD}:${PWD}" \
		-w "${PWD}" \
		-e "WEBSEED=$(WEBSEED)" \
		"$(DOCKER_IMAGE_BUILDER)" \
		scripts/torrent.sh \
		debian-bookworm

.PHONY: ansible
ansible:
	docker run \
		--rm \
		-v "${PWD}/packer/ansible:/ansible" \
		-u `id -u`:`id -g` \
		-e "WEBSEED=$(WEBSEED)" \
		-v "${SSH_AUTH_SOCK}:/var/run/ssh_auth_sock" \
		-e "SSH_AUTH_SOCK=/var/run/ssh_auth_sock" \
		-w /ansible \
		"$(DOCKER_IMAGE_BUILDER)" \
			/usr/bin/ansible-playbook \
			-i inventories \
			$(if $(ANSIBLE_TAGS),-t $(ANSIBLE_TAGS),) \
			-l $(ANSIBLE_LIMIT) \
			--ssh-extra-args="-o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no" \
			playbook.yml

# updates the initramfs
# only used for development
images/debian-bookworm.initramfs.dev: tmp/initramfs-extracted/debian-bookworm packer/initramfs/labsync
	cp packer/initramfs/labsync tmp/initramfs-extracted/debian-bookworm/scripts/labsync
	(cd tmp/initramfs-extracted/debian-bookworm && find . | cpio -H newc -o | gzip > $(CWD)/images/debian-bookworm.initramfs.dev)

tmp:
	[ ! -d "$@" ] && mkdir "$@" || true

tmp/initramfs-extracted/debian-bookworm: images/debian-bookworm.initramfs
	rm -rf tmp/initramfs-extracted/debian-bookworm
	mkdir -p tmp/initramfs-extracted/debian-bookworm
	(cd tmp/initramfs-extracted/debian-bookworm && zcat "$(CWD)/images/debian-bookworm.initramfs" | cpio -i)
	touch tmp/initramfs-extracted/debian-bookworm

$(qemu_disk): tmp
	qemu-img create "$@" 20G

tmp/netboot.tar.gz: tmp
	wget -c -O "$@" https://cdn-aws.deb.debian.org/debian/dists/bookworm/main/installer-amd64/current/images/netboot/netboot.tar.gz
	touch "$@"

tmp/tftproot: tmp/netboot.tar.gz
	[ ! -d tmp/tftproot ] && mkdir tmp/tftproot || true
	tar -xzf "$<" -C "$@"
	rm tmp/tftproot/debian-installer/amd64/boot-screens/txt.cfg

tmp/tftproot/images: tmp/tftproot
	ln -s ../../images tmp/tftproot/images

tmp/tftproot/debian-installer/amd64/boot-screens/txt.cfg: txt.cfg
	ln -s ../../../../../txt.cfg "$@"


/sys/devices/virtual/net/$(qemu_ifname_tap):
	sudo ip tuntap add dev $(qemu_ifname_tap) mode tap user $(USER)

/sys/devices/virtual/net/$(qemu_ifname_br):
	sudo brctl addbr $(qemu_ifname_br)

/sys/devices/virtual/net/$(qemu_ifname_br)/brif/$(qemu_ifname_tap): /sys/devices/virtual/net/$(qemu_ifname_tap) /sys/devices/virtual/net/$(qemu_ifname_br)
	sudo brctl addif $(qemu_ifname_br) $(qemu_ifname_tap)

.PHONY: docker-services
docker-services: qemu-network
	docker-compose up -d

.PHONY: qemu-network
qemu-network:  /sys/devices/virtual/net/$(qemu_ifname_br)/brif/$(qemu_ifname_tap)
	if ! ip addr show dev $(qemu_ifname_br) | grep -F "$(qemu_host_ip)/$(qemu_netmask)"; then \
		sudo ip addr add $(qemu_host_ip)/$(qemu_netmask) dev $(qemu_ifname_br); \
	fi
	sudo ip link set $(qemu_ifname_tap) up
	sudo ip link set $(qemu_ifname_br) up
	if ! sudo iptables -t nat -L POSTROUTING | grep -q "$(qemu_network)/$(qemu_netmask)"; then \
		sudo iptables -t nat -A POSTROUTING -s $(qemu_network)/$(qemu_netmask) ! -o $(qemu_ifname_br) -j MASQUERADE; \
	fi

.PHONY: qemu
qemu: qemu-network $(qemu_disk) docker-services
	qemu-system-x86_64 \
		-kernel "images/$(qemu_kernel)" \
		-initrd "images/$(qemu_initramfs)" \
		-drive format=raw,file="$(qemu_disk)" \
		-append "boot=labsync labsync_disk=sda labsync_partsize_boot=512 labsync_torrent=http://10.2.2.1/$(qemu_torrent) quiet vga=792 ip=$(qemu_vm_ip):::255.255.255.0:$(qemu_hostname):ens3:off labsync_wait=pause labsync_debug=1" \
		-enable-kvm \
		-m 1G \
		-net nic \
		-net tap,ifname=$(qemu_ifname_tap),script=no,downscript=no \
		-boot order=n

.PHONY: qemu-localboot
qemu-localboot: qemu-network $(qemu_disk)
	qemu-system-x86_64 \
		-drive format=raw,file="$(qemu_disk)" \
		-enable-kvm \
		-m 1G \
		-net nic \
		-net tap,ifname=$(qemu_ifname_tap),script=no,downscript=no \
		-boot order=c

.PHONY: qemu-tftp docker-services
qemu-tftp: tmp/tftproot tmp/tftproot/images tmp/tftproot/debian-installer/amd64/boot-screens/txt.cfg qemu-network $(qemu_disk)
	qemu-system-x86_64 \
		-net nic -net tap,ifname=$(qemu_ifname_tap),script=no,downscript=no \
		-net nic -net user,tftp=.,bootfile=/tmp/tftproot/pxelinux.0 \
		-enable-kvm \
		-m 1G \
		-drive format=raw,file="$(qemu_disk)" \
		-boot n

